﻿using BudRogers.Gameplay;
using UnityEngine;

namespace BudRogers.Ships.Motors
{

    public class Motor : MonoBehaviour
    {
        #region Serialized.
        /// <summary>
        /// How fast the ship may move.
        /// </summary>
        [Tooltip("How fast the ship may move.")]
        [SerializeField]
        private float _moveRate = 1f;
        #endregion

        private void Start()
        {
            SnapToBoundsCenter();
        }

        private void Update()
        {
            Move();
        }

        /// <summary>
        /// Snaps the ship to the center of the gameplay bounds.
        /// </summary>
        private void SnapToBoundsCenter()
        {
            Vector3 next = GameplayDependencies.WorldBounds.ReturnCenter();
            transform.position = new Vector3(next.x, next.y, transform.position.z);
        }

        /// <summary>
        /// Moves the ship within the gameplay bounds.
        /// </summary>
        private void Move()
        {
            float horizontal = Input.GetAxisRaw("Horizontal");
            float vertical = Input.GetAxisRaw("Vertical");
            //No input.
            if (horizontal == 0f && vertical == 0f)
                return;

            //Get next position and clamp to bounds.
            Vector3 step = new Vector3(horizontal, vertical, 0f) * (_moveRate * Time.deltaTime);
            Vector3 next = transform.position + step;
            GameplayDependencies.WorldBounds.ClampToBounds(ref next);
            //Update transform position.
            transform.position = next;
        }
    }


}