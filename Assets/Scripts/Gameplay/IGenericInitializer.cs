﻿namespace BudRogers.Gameplay
{

    /// <summary>
    /// Used to initialize gameObjects with random parameters.
    /// </summary>
    public interface IGenericInitializer
    {
        void GenericInitialize(params object[] data);
    }

}