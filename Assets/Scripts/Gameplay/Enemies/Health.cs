﻿using UnityEngine;

namespace BudRogers.Gameplay.Enemies
{

    public class Health : MonoBehaviour
    {
        #region Serialized.
        /// <summary>
        /// Base health for this object.
        /// </summary>
        [Tooltip("Base health for this object.")]
        [SerializeField]
        private float _health = 15f;
        #endregion

        #region Private.
        /// <summary>
        /// VisibilityMonitor on this object.
        /// </summary>
        private VisibilityMonitor _visibilityMonitor = null;
        /// <summary>
        /// ImpactMonitor on this object.
        /// </summary>
        private ImpactMonitor _impactMonitor = null;
        #endregion

        private void Awake()
        {
            FirstInitialize();
        }

        /// <summary>
        /// Initializes this script for use.
        /// </summary>
        private void FirstInitialize()
        {
            _visibilityMonitor = GetComponent<VisibilityMonitor>();
            _impactMonitor = GetComponent<ImpactMonitor>();
            _impactMonitor.OnTriggerEnter2D += ImpactMonitor_OnTriggerEnter2D;
        }

        private void ImpactMonitor_OnTriggerEnter2D(Collider2D obj)
        {
            throw new System.NotImplementedException();
        }

    }

}
