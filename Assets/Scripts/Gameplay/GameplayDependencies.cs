﻿//using FirstGearGames.FastObjectPool;
using FirstGearGames.FastObjectPool;
using UnityEngine;

namespace BudRogers.Gameplay
{

    public class GameplayDependencies : MonoBehaviour
    {
        #region Serialized.
        /// <summary>
        /// 
        /// </summary>
        [Tooltip("WorldBounds for this gameplay.")]
        [SerializeField]
        private WorldBounds _worldBounds = null;
        /// <summary>
        /// WorldBounds for this gameplay.
        /// </summary>
        public static WorldBounds WorldBounds => _instance._worldBounds;
        #endregion

        #region Private.
        /// <summary>
        /// Instance of this script.
        /// </summary>
        private static GameplayDependencies _instance;
        #endregion

        private void Awake()
        {
            FirstInitialize();
        }

        /// <summary>
        /// Initializes this script for use.
        /// </summary>
        private void FirstInitialize()
        {
            if (_instance != null)
            {
                Destroy(gameObject);
                return;
            }

            _instance = this;
        }
        
    }


}