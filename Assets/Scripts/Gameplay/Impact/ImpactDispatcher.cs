﻿using System;
using UnityEngine;

namespace BudRogers.Gameplay
{

    public class ImpactDispatcher : MonoBehaviour
    {
        #region Public.
        /// <summary>
        /// Called when a collision enters.
        /// </summary>
        internal event Action<Collision2D> Relay_OnCollisionEnter2D;
        /// <summary>
        /// Called when a collision exits.
        /// </summary>
        internal event Action<Collision2D> Relay_OnCollisionExit2D;
        /// <summary>
        /// Called when a trigger enters.
        /// </summary>
        internal event Action<Collider2D> Relay_OnTriggerEnter2D;
        /// <summary>
        /// Called when a trigger exits.
        /// </summary>
        internal event Action<Collider2D> Relay_OnTriggerExit2D;
        #endregion

        private void OnCollisionEnter2D(Collision2D collision)
        {
            Relay_OnCollisionEnter2D?.Invoke(collision);
        }

        private void OnCollisionExit2D(Collision2D collision)
        {
            Relay_OnCollisionExit2D?.Invoke(collision);
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            Relay_OnTriggerEnter2D?.Invoke(collision);
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            Relay_OnTriggerExit2D?.Invoke(collision);
        }
    }


}