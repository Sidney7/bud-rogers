﻿using System;
using UnityEngine;

namespace BudRogers.Gameplay
{

    public class ImpactMonitor : MonoBehaviour
    {
        #region Public.
        /// <summary>
        /// Called when a collision enters.
        /// </summary>
        internal event Action<Collision2D> OnCollisionEnter2D;
        /// <summary>
        /// Called when a collision exits.
        /// </summary>
        internal event Action<Collision2D> OnCollisionExit2D;
        /// <summary>
        /// Called when a trigger enters.
        /// </summary>
        internal event Action<Collider2D> OnTriggerEnter2D;
        /// <summary>
        /// Called when a trigger exits.
        /// </summary>
        internal event Action<Collider2D> OnTriggerExit2D;
        #endregion

        private void Awake()
        {
            FirstInitialize();
        }

        /// <summary>
        /// Initializes this script for use.
        /// </summary>
        private void FirstInitialize()
        {
            ImpactDispatcher[] ids = GetComponentsInChildren<ImpactDispatcher>();
            foreach (ImpactDispatcher item in ids)
            {
                item.Relay_OnCollisionEnter2D += CollisionDispatcher_Relay_OnCollisionEnter2D;
                item.Relay_OnCollisionExit2D += CollisionDispatcher_Relay_OnCollisionExit2D;
                item.Relay_OnTriggerEnter2D += Item_Relay_OnTriggerEnter2D;
                item.Relay_OnTriggerExit2D += Item_Relay_OnTriggerExit2D;
            }
        }

        private void Item_Relay_OnTriggerExit2D(Collider2D obj)
        {
            throw new NotImplementedException();
        }

        private void Item_Relay_OnTriggerEnter2D(Collider2D obj)
        {
            throw new NotImplementedException();
        }

        private void CollisionDispatcher_Relay_OnCollisionExit2D(Collision2D obj)
        {
            throw new NotImplementedException();
        }

        private void CollisionDispatcher_Relay_OnCollisionEnter2D(Collision2D obj)
        {
            throw new NotImplementedException();
        }
      

    }


}