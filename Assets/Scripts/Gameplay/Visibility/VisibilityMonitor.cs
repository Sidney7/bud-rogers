﻿using System;
using UnityEngine;

namespace BudRogers.Gameplay
{

    public class VisibilityMonitor : MonoBehaviour
    {
        #region Public.
        /// <summary>
        /// Called when any renderer becomes visible.
        /// </summary>
        internal event Action OnBecameVisibile;
        /// <summary>
        /// Called when all renderers are invisible.
        /// </summary>
        internal event Action OnBecameInvisibile;
        /// <summary>
        /// True if any renderer is visible.
        /// </summary>
        internal bool Visible => (_visibleCount > 0);
        #endregion

        #region Private.
        /// <summary>
        /// Number of renders which are visible for this object.
        /// </summary>
        private int _visibleCount = 0;
        #endregion

        private void Awake()
        {
            FirstInitialize();
        }

        /// <summary>
        /// Initializes this script for use.
        /// </summary>
        private void FirstInitialize()
        {
            VisibilityDispatcher[] vds = GetComponentsInChildren<VisibilityDispatcher>();
            foreach (VisibilityDispatcher item in vds)
            {
                item.Relay_OnBecameInvisibile += VisibilityDispatcher_Relay_OnBecameInvisibile;
                item.Relay_OnBecameVisibile += VisibilityDispatcher_Relay_OnBecameVisibile;
            }
        }

        private void VisibilityDispatcher_Relay_OnBecameVisibile()
        {
            _visibleCount += 1;
            OnBecameVisibile?.Invoke();
        }

        private void VisibilityDispatcher_Relay_OnBecameInvisibile()
        {
            _visibleCount -= 1;
            if (_visibleCount == 0)
                OnBecameInvisibile?.Invoke();
        }

    }


}