﻿using System;
using UnityEngine;

namespace BudRogers.Gameplay
{

    public class VisibilityDispatcher : MonoBehaviour
    {
        #region Public.
        /// <summary>
        /// Called when the renderer becomes visible.
        /// </summary>
        internal event Action Relay_OnBecameVisibile;
        /// <summary>
        /// Called when the renderer becomes invisible.
        /// </summary>
        internal event Action Relay_OnBecameInvisibile;
        #endregion

        private void OnBecameInvisible()
        {
            Relay_OnBecameInvisibile?.Invoke();
        }

        private void OnBecameVisible()
        {
            Relay_OnBecameVisibile?.Invoke();
        }
    }


}