﻿using UnityEngine;

namespace BudRogers.Gameplay.ObjectSpawners
{


    public abstract class RandomObjectSpawner : MonoBehaviour
    {
        #region Serialized.
        /// <summary>
        /// Use SpawnObject.
        /// </summary>
        [Tooltip("Objects to spawn. One object is selected randomly at a time.")]
        [SerializeField]
        private GameObject[] _spawnObjects = new GameObject[0];
        /// <summary>
        /// Objects to spawn. One object is selected randomly at a time.
        /// </summary>
        protected GameObject[] SpawnObjects { get { return _spawnObjects; } }
        /// <summary>
        /// Use SpawnFrequency.
        /// </summary>
        [Tooltip("How often to spawn the objects. Only spawns once if set to -1f.")]
        [SerializeField]
        private float _spawnFrequency = 4f;
        /// <summary>
        /// How often to spawn the objects. Only spawns once if set to -1f.
        /// </summary>
        protected float SpawnFrequency { get { return _spawnFrequency; } }
        /// <summary>
        /// Use FrequencyVariance.
        /// </summary>
        [Tooltip("Percentage of time in which SpawnFrequency can vary in either direction.")]
        [Range(0f, 1f)]
        [SerializeField]
        private float _frequencyVariance = 0.5f;
        /// <summary>
        /// Percentage of time in which SpawnFrequency can vary in either direction.
        /// </summary>
        protected float FrequencyVariance { get { return _frequencyVariance; } }
        /// <summary>
        /// Use RandomStartSpawn.
        /// </summary>
        [Tooltip("True to set first spawn to a random start time within the SpawnFrequency.")]
        [SerializeField]
        private bool _randomStartSpawn = true;
        /// <summary>
        /// True to set first spawn to a random start time within the SpawnFrequency.
        /// </summary>
        protected bool RandomStartSpawn { get { return _randomStartSpawn; } }
        /// <summary>
        /// Use SpawnRange.
        /// </summary>
        [Tooltip("Area around the center of this gameObject in which units can spawn. Used differently among inherited classes.")]
        [SerializeField]
        private Vector3 _spawnRange = new Vector3(200f, 200f, 1f);
        /// <summary>
        /// Area around the center of this gameObject in which units can spawn. Used differently among inherited classes.
        /// </summary>
        public Vector3 SpawnRange { get { return _spawnRange; } }
        #endregion


        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireCube(transform.position, SpawnRange);
        }

    }


}