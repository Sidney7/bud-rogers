﻿using BudRogers.Gameplay.MovingObjects;
using FirstGearGames.FastObjectPool;
using FirstGearGames.Utilities.Maths;
using FirstGearGames.Utilities.Monos;
using FirstGearGames.Utilities.Objects;
using FirstGearGames.Utilities.Structures;
using System;
using System.Text;
using UnityEngine;

namespace BudRogers.Gameplay.ObjectSpawners
{


    /// <summary>
    /// Uses the PlayArea to determine edges of the scene, and spawns objects on the edges in accordance to specified settings.
    /// </summary>
    public class EdgeSpawner : RandomObjectSpawner
    {
        #region Serialized.
        /// <summary>
        /// Direction to try and force the spawned object to move. Does nothing as Unset.
        /// </summary>
        [Tooltip("Direction to try and force the spawned object to move. Does nothing as Unset.")]
        [SerializeField]
        private HorizontalDirections _moveDirection = HorizontalDirections.Unset;
        /// <summary>
        /// Random range of objects to spawn. Only applies if not using a SpawnFrequency.
        /// </summary>
        [Tooltip("Random range of objects to spawn. Only applies if not using a SpawnFrequency.")]
        [SerializeField]
        private IntRange m_spawnCount = new IntRange(3, 9);
        /// <summary>
        /// True to force objects outside of edge view using their ObjectSizer component. If no ObjectSizer component is present this option fails.
        /// </summary>
        [Tooltip("True to force objects outside of edge view using their ObjectSizer component. If no ObjectSizer component is present this option fails.")]
        [SerializeField]
        private bool m_forceOutsideEdge = true;
        #endregion

        #region Private.
        /// <summary>
        /// Next time to spawn an object.
        /// </summary>
        private float m_nextSpawnTime = 0f;
        #endregion

        private void Start()
        {
            if (ValidateSettings())
                CheckFirstSpawn();
            else
                this.enabled = false;
        }

        private void Update()
        {
            CheckSpawnObject();
        }

        /// <summary>
        /// Ensures all required data is present to run this script.
        /// </summary>
        private bool ValidateSettings()
        {
            //Becomes occupied with error messages as they occur.
            StringBuilder sb = new StringBuilder();

            //If no spawn frequency.
            if (base.SpawnFrequency <= 0f)
            {
                //Invalid spawn count.
                if (m_spawnCount.Minimum < 0 || m_spawnCount.Maximum <= 0)
                    sb.Append("Invalid SpawnCount range. Minimum must be 0 or higher, Maximum must be 1 or higher.");
            }

            //If an error occurred.
            if (sb.Length != 0)
            {
                //Add the header.
                sb.Insert(0, Debugs.FormatTransform(transform) + " RandomRangeObjectSpawner -> ValidateSettings -> One or more errors! " + Environment.NewLine);
                Debug.LogError(sb.ToString());
                return false;
            }
            //No errors.
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Checks how to handle the first spawn type.
        /// </summary>
        private void CheckFirstSpawn()
        {
            /* A frequency less than or equal to 0f indicates to only spawn once.
             * If this is the case then force a spawn then
             * disable the script. */
            if (base.SpawnFrequency <= 0f)
            {
                m_nextSpawnTime = -1f;
                //Randomly determine a spawn count then spawn items.
                int count = Ints.RandomInclusiveRange(m_spawnCount.Minimum, m_spawnCount.Maximum);
                for (int i = 0; i < count; i++)
                {
                    CheckSpawnObject();
                }

                this.enabled = false;
            }
            //Positive frequency.
            else
            {
                //If to delay first spawn by setting a spawn time from current time.
                if (base.RandomStartSpawn)
                    SetNextSpawnTime(true);
            }
        }

        /// <summary>
        /// Sets the next spawn time using preset fields and current time.
        /// </summary>
        /// <param name="fullRange">Value should be between instant(0f) and maximum possible value with variance.</param>
        private void SetNextSpawnTime(bool fullRange = false)
        {
            //If not only spawn once.
            if (base.SpawnFrequency > 0f)
            {
                float result = 0f;
                if (fullRange)
                {
                    float maxTime = base.SpawnFrequency * Floats.RandomInclusiveRange(1f, 1f + base.FrequencyVariance);
                    result = Floats.RandomInclusiveRange(0f, maxTime);
                }
                else
                {
                    result = base.SpawnFrequency.Variance(base.FrequencyVariance);
                }
                m_nextSpawnTime = Time.time + result;
            }
        }

        private void CheckSpawnObject()
        {
            //Not enough time has passed.
            if (Time.time < m_nextSpawnTime)
                return;

            SetNextSpawnTime();

            //Pick an object to spawn.
            int index = Ints.RandomExclusiveRange(0, base.SpawnObjects.Length);

            GameObject obj = ObjectPool.Retrieve(base.SpawnObjects[index]);
            /* Set positions as where to spawn the object. */
            float yRange = Floats.RandomInclusiveRange(-base.SpawnRange.y / 2f, base.SpawnRange.y / 2f);
            float yPosition = transform.position.y + yRange;
            float xPosition = transform.position.x;
            //If to force outside the edge.
            if (m_forceOutsideEdge)
            {
                //Check for an object sizer.
                ObjectSizer sizer = obj.GetComponent<ObjectSizer>();
                //Can only proceed if a sizer is found.
                if (sizer != null)
                {
                    //If this edge spawner is right of the play area.
                    if (transform.position.x > Camera.main.transform.position.x)
                        xPosition += sizer.HalfSize.x;
                    //If left of play area center.
                    else
                        xPosition -= sizer.HalfSize.x;
                }
            }
            //Set game objects depth.
            float zPosition = transform.position.z;

            obj.transform.position = new Vector3(xPosition, yPosition, zPosition);
            SetMoveDirection(obj);
        }

        /// <summary>
        /// Tries to intialize the gameObjects move direction.
        /// </summary>
        /// <param name="obj"></param>
        private void SetMoveDirection(GameObject obj)
        {
            if (_moveDirection == HorizontalDirections.Unset)
                return;

            IGenericInitializer initializer = obj.GetComponent<IGenericInitializer>();
            //If initializer exist try to initialize.
            if (initializer != null)
                initializer.GenericInitialize(_moveDirection);
        }


    }


}