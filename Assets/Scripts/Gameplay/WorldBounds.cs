﻿using UnityEngine;

namespace BudRogers.Gameplay
{

    public class WorldBounds : MonoBehaviour
    {

        #region Serialized.
        /// <summary>
        /// Offset from transform center for bounds.
        /// </summary>
        [Tooltip("Offset from camera center for bounds.")]
        [SerializeField]
        private Vector2 _offset = Vector2.zero;
        /// <summary>
        /// Size of bounds.
        /// </summary>
        [Tooltip("Size of bounds.")]
        [SerializeField]
        private Vector2 _size = new Vector2(500f, 200f);
        #endregion
        
        /// <summary>
        /// Returns center of the bounds.
        /// </summary>
        /// <returns></returns>
        internal Vector3 ReturnCenter()
        {
            Transform cameraTransform = Camera.main.transform;
            Vector2 center = new Vector2(cameraTransform.position.x, cameraTransform.position.y) + _offset;
            return center;
        }

        /// <summary>
        /// Clamps a transform within the bounds.
        /// </summary>
        /// <param name="t"></param>
        internal void ClampToBounds(ref Vector3 targetPosition)
        {
            Transform cameraTransform = Camera.main.transform;
            Vector2 center = new Vector2(cameraTransform.position.x, cameraTransform.position.y) + _offset;
            Vector2 upper = new Vector2(center.x + (_size.x / 2f), center.y + (_size.y / 2f));
            Vector2 lower = new Vector2(center.x - (_size.x / 2f), center.y - (_size.y / 2f));

            targetPosition.x = Mathf.Clamp(targetPosition.x, lower.x, upper.x);
            targetPosition.y = Mathf.Clamp(targetPosition.y, lower.y, upper.y);
        }

        private void OnDrawGizmosSelected()
        {
            Vector3 center = Camera.main.transform.position + new Vector3(_offset.x, _offset.y, 0f);
            Gizmos.DrawWireCube(center, new Vector3(_size.x, _size.y, 1f));
        }
    }


}