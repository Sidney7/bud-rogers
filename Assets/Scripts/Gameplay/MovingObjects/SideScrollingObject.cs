﻿using FirstGearGames.FastObjectPool;
using FirstGearGames.Utilities.Maths;
using FirstGearGames.Utilities.Monos;
using FirstGearGames.Utilities.Objects;
using FirstGearGames.Utilities.Structures;
using System;
using System.Text;
using UnityEngine;

namespace BudRogers.Gameplay.MovingObjects
{

    [RequireComponent(typeof(ObjectSizer))]
    public class SideScrollingObject : MonoBehaviour, IGenericInitializer
    {
        #region Serialized.
        /// <summary>
        /// True to randomly flip which direction this gameObject faces. Changes the Y eulerAngle on the object.
        /// </summary>
        [Tooltip("True to randomly flip which direction this gameObject faces. Changes the Y eulerAngle on the object.")]
        [SerializeField]
        private bool _randomlyFlipFacing = true;
        /// <summary>
        /// Default direction to move on spawn.
        /// </summary>
        [Tooltip("Default direction to move on spawn.")]
        [SerializeField]
        private HorizontalDirections _moveDirection = HorizontalDirections.Left;
        /// <summary>
        /// How quickly to move before considering variance.
        /// </summary>
        [Tooltip("How quickly to move before considering variance.")]
        [SerializeField]
        private float _baseMoveSpeed = 5f;
        /// <summary>
        /// Percentage in which the move speed may vary in either direction.
        /// </summary>
        [Tooltip("Percentage in which the move speed may vary in either direction.")]
        [Range(0f, 1f)]
        [SerializeField]
        private float _baseMoveSpeedVariance = 0.3f;
        /// <summary>
        /// Header for scaling related options.
        /// </summary>
        [Header("Scaling")]
        /// <summary>
        /// Range in which this gameObject can scale.
        /// </summary>
        [SerializeField]
        private FloatRange _scaleRange = new FloatRange(0.2f, 1f);
        /// <summary>
        /// Percentage in which the scale my vary in either direction.
        /// </summary>
        [Tooltip("Percentage in which the scale my vary in either direction.")]
        [SerializeField]
        private float _scaleVariance = 0.35f;
        #endregion

        #region Private.
        /// <summary>
        /// Move speed after variance is applied.
        /// </summary>
        private float _adjustedMoveSpeed = 0f;
        /// <summary>
        /// Half the size of the objects object sizer. Remains 0f if no object sizer is present.
        /// </summary>
        private float _sizeX = 0f;
        /// <summary>
        /// Becomes true when the script fully initializes and starts.
        /// </summary>
        private bool _initialized = false;
        /// <summary>
        /// When beyond this point the object will end.
        /// </summary>
        private float _endX = 0f;
        #endregion

        private void Awake()
        {
            ObjectSizer objectSizer = GetComponent<ObjectSizer>();
            _sizeX = objectSizer.UnscaledSize.x;
        }

        private void Start()
        {
            ValidateSettings();
        }

        private void OnDisable()
        {
            _initialized = false;
        }

        private void Update()
        {
            Move();
        }

        /// <summary>
        /// Ensures all required data is present to run this script.
        /// </summary>
        private bool ValidateSettings()
        {
            //Becomes occupied with error messages as they occur.
            StringBuilder sb = new StringBuilder();

            if (_moveDirection == HorizontalDirections.Unset)
                sb.Append("BaseMoveDirection cannot be unset.");

            //If an error occurred.
            if (sb.Length != 0)
            {
                //Add the header.
                sb.Insert(0, Debugs.FormatTransform(transform) + " SimpleMovingObject -> ValidateSettings -> One or more errors! " + Environment.NewLine);
                Debug.LogError(sb.ToString());
                return false;
            }
            //No errors.
            else
            {
                return true;
            }
        }


        /// <summary>
        /// Sets the gameObjects facing based on settings and DirectionMultiplier.
        /// </summary>
        private void SetFacing()
        {
            //If to randomly flip which way the object is facing.
            if (_randomlyFlipFacing)
            {
                if (Ints.RandomInclusiveRange(0, 1) == 0)
                    transform.eulerAngles = new Vector3(transform.eulerAngles.x, 180f, transform.eulerAngles.z);
                else
                    transform.eulerAngles = new Vector3(transform.eulerAngles.x, 0f, transform.eulerAngles.z);
            }
            //If not randomly flip then set facing based on direction multiplier.
            else
            {
                //Moving right.
                if (_moveDirection == HorizontalDirections.Right)
                    transform.eulerAngles = new Vector3(transform.eulerAngles.x, 0f, transform.eulerAngles.z);
                //Moving left.
                else
                    transform.eulerAngles = new Vector3(transform.eulerAngles.x, 180f, transform.eulerAngles.z);
            }
        }

        /// <summary>
        /// Sets the move speed in accordance to movespeed settings.
        /// </summary>
        private void SetMoveSpeed()
        {
            _baseMoveSpeed.Variance(_baseMoveSpeedVariance, ref _adjustedMoveSpeed);
        }

        /// <summary>
        /// Scales this gameObject using scaling settings.
        /// </summary>
        private void ScaleObject()
        {
            float depth = transform.position.z;
            float scale = Floats.RandomInclusiveRange(_scaleRange.Minimum, _scaleRange.Maximum);

            //Add variance to scale.
            scale.Variance(_scaleVariance, ref scale);
            transform.localScale = new Vector3(scale, scale, 1f);
        }

        /// <summary>
        /// Moves this gameObject with set values.
        /// </summary>
        private void Move()
        {
            //Cannot proceed if not initialized.
            if (!_initialized)
                return;

            float rate = _adjustedMoveSpeed * (float)_moveDirection * Time.deltaTime;
            transform.position += new Vector3(rate, 0f, 0f);

            CheckAtDestination();
        }

        /// <summary>
        /// Checks if this object is at it's destination. This class has no specific destination, so destination is considered off screen.
        /// </summary>
        private void CheckAtDestination()
        {
            //If moving left.
            if (_moveDirection == HorizontalDirections.Left)
            {
                if (transform.position.x < _endX)
                    ArrivedAtDestination();
            }
            //Else, must be moving right.
            else
            {
                if (transform.position.x > _endX)
                    ArrivedAtDestination();
            }
        }

        /// <summary>
        /// Called when this object arrives at it's destination. This class has no specific destination, so destination is considered off screen.
        /// </summary>
        private void ArrivedAtDestination()
        {
            //Try to pool the object. 
            ObjectPool.Store(gameObject);
        }

        /// <summary>
        /// Initializes the script using IGeneralInitializer.
        /// </summary>
        /// <param name="data"></param>
        public void GenericInitialize(params object[] data)
        {
            if (data == null)
                return;

            if (data.Length > 0)
            {
                //Check for valid type.
                Type dataType = data[0].GetType();
                if (dataType == typeof(HorizontalDirections))
                    _moveDirection = (HorizontalDirections)data[0];

                float cameraX = Camera.main.transform.position.x;
                float cameraOffset = cameraX - transform.position.x;
                _endX = cameraX + cameraOffset;

                SetMoveSpeed();
                ScaleObject();
                SetFacing();

                _initialized = true;
            }
        }
    }


}