﻿namespace BudRogers.Gameplay.MovingObjects
{
    /// <summary>
    /// Directions a unit may be moving.
    /// </summary>
    public enum HorizontalDirections : sbyte
    {
        Unset = 0,
        Left = -1,
        Right = 1
    }


}