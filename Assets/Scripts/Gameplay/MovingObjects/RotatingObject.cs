﻿using FirstGearGames.FastObjectPool;
using FirstGearGames.Utilities.Maths;
using FirstGearGames.Utilities.Monos;
using FirstGearGames.Utilities.Objects;
using FirstGearGames.Utilities.Structures;
using System;
using System.Text;
using UnityEngine;

namespace BudRogers.Gameplay.MovingObjects
{

    [RequireComponent(typeof(ObjectSizer))]
    public class RotatingObject : MonoBehaviour
    {
        #region Serialized.
        /// <summary>
        /// True to randomly flip which direction this gameObject rotates.
        /// </summary>
        [Tooltip("True to randomly flip which direction this gameObject rotates.")]
        [SerializeField]
        private bool _randomlyFlipRotation = true;
        /// <summary>
        /// How quickly to rotate before considering variance.
        /// </summary>
        [Tooltip("How quickly to rotate before considering variance.")]
        [SerializeField]
        private float _baseRotateSpeed = 5f;
        /// <summary>
        /// Percentage in which the rotate speed may vary in either direction.
        /// </summary>
        [Tooltip("Percentage in which the rotate speed may vary in either direction.")]
        [Range(0f, 1f)]
        [SerializeField]
        private float _baseRotateSpeedVariance = 0.3f;
        /// <summary>
        /// Default direction to rotate on spawn.
        /// </summary>
        [Tooltip("Default direction to rotate on spawn.")]
        [SerializeField]
        private HorizontalDirections _rotateDirection = HorizontalDirections.Left;
        #endregion

        #region Private.
        /// <summary>
        /// Rotate speed after variance is applied.
        /// </summary>
        private float _adjustedRotateSpeed = 0f;
        /// <summary>
        /// Direction to rotate.
        /// </summary>
        private float _rotateDirectionMultiplier = 1f;
        #endregion


        private void OnEnable()
        {
            if (_randomlyFlipRotation)
                _rotateDirectionMultiplier = Floats.RandomlyFlip(1f);
            else
                _rotateDirectionMultiplier = (float)_rotateDirection;
            _baseRotateSpeed.Variance(_baseRotateSpeedVariance, ref _adjustedRotateSpeed);
        }

        private void Update()
        {
            Rotate();
        }

        /// <summary>
        /// Moves this gameObject with set values.
        /// </summary>
        private void Rotate()
        {
            float rate = _adjustedRotateSpeed * _rotateDirectionMultiplier * Time.deltaTime;
            transform.Rotate(new Vector3(0f, 0f, rate));
        }

    }


}