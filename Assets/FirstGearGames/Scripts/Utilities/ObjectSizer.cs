﻿using FirstGearGames.Utilities.Maths;
using Sirenix.OdinInspector;
using UnityEngine;

namespace FirstGearGames.Utilities.Monos
{



    public class ObjectSizer : MonoBehaviour
    {
        #region Types.
        /// <summary>
        /// Types of gizmos to draw.
        /// </summary>
        public enum ObjectSizerGizmosDrawTypes
        {
            Vector = 1,
            Radius = 2
        }
        #endregion

        #region Public.
        /// <summary>
        /// Where the area center is in world space.
        /// </summary>
        public Vector3 Center
        {
            get
            {
                return (transform.position + Offset);
            }
        }
        /// <summary>
        /// Radius of the area.
        /// </summary>
        public float Radius
        {
            get { return Size.x / 2f; }
        }
        /// <summary>
        /// Unscaled radius of the area.
        /// </summary>
        public float UnscaledRadius
        {
            get { return UnscaledSize.x / 2f; }
        }
        /// <summary>
        /// Height of the area.
        /// </summary>
        public float Height
        {
            get { return Size.y; }
        }
        /// <summary>
        /// Offset for ObjectSizer.
        /// </summary>
        public Vector3 Offset
        {
            get
            {
                if (_autoAlign)
                    return new Vector3(0f, (Size.y / 2f), 0f);
                else
                    return new Vector3(_offset.x * transform.localScale.x, _offset.y * transform.localScale.y, _offset.z * transform.localScale.z);
            }
            set
            {
                //If setting manually then disable auto center.
                _autoAlign = false;
                _offset = value;
            }
        }
        /// <summary>
        /// Offset for ObjectSizer ignoring scale.
        /// </summary>
        public Vector3 UnscaledOffset
        {
            get
            {
                if (_autoAlign)
                {
                    switch (DrawType)
                    {
                        case ObjectSizerGizmosDrawTypes.Vector:
                            return new Vector3(0f, _vectorSize.y / 2f, 0f);
                        case ObjectSizerGizmosDrawTypes.Radius:
                            return new Vector3(0f, _radiusSize / 2f, 0f);
                        default:
                            return Vector3.zero;
                    }
                }
                else
                {
                    return new Vector3(_offset.x, _offset.y, _offset.z);
                }
            }
        }
        /// <summary>
        /// Size of the area.
        /// </summary>
        public Vector3 Size
        {
            get
            {
                switch (DrawType)
                {
                    case ObjectSizerGizmosDrawTypes.Vector:
                        return new Vector3(_vectorSize.x * transform.localScale.x, _vectorSize.y * transform.localScale.y, _vectorSize.z * transform.localScale.z);
                    case ObjectSizerGizmosDrawTypes.Radius:
                        return new Vector3(_radiusSize * 2f * transform.localScale.x, _radiusSize * 2f * transform.localScale.y, _radiusSize * 2f * transform.localScale.z);
                    default:
                        return Vector3.zero;
                }
            }
            private set
            {
                _vectorSize = value;
            }
        }
        /// <summary>
        /// Size of the area ignoring scale alterations.
        /// </summary>
        public Vector3 UnscaledSize
        {
            get
            {
                return _vectorSize;
            }
        }
        /// <summary>
        /// Half size of the area.
        /// </summary>
        public Vector3 HalfSize
        {
            get
            {
                switch (DrawType)
                {
                    case ObjectSizerGizmosDrawTypes.Vector:
                        return new Vector3(_vectorSize.x * 0.5f * transform.localScale.x, _vectorSize.y * 0.5f * transform.localScale.y, _vectorSize.z * 0.5f * transform.localScale.z);
                    case ObjectSizerGizmosDrawTypes.Radius:
                        return new Vector3(_radiusSize * transform.localScale.x, _radiusSize * transform.localScale.y, _radiusSize * transform.localScale.z);
                    default:
                        return Vector3.zero;
                }
            }
        }
        /// <summary>
        /// HalfSize of the area ignoring scale alterations.
        /// </summary>
        public Vector3 UnscaledHalfSize
        {
            get
            {
                switch (DrawType)
                {
                    case ObjectSizerGizmosDrawTypes.Vector:
                        return new Vector3(_vectorSize.x * 0.5f, _vectorSize.y * 0.5f, _vectorSize.z * 0.5f);
                    case ObjectSizerGizmosDrawTypes.Radius:
                        return new Vector3(_radiusSize, _radiusSize, _radiusSize);
                    default:
                        return Vector3.zero;
                }
            }
        }
        #endregion

        #region Serialized.
        /// <summary>
        /// Use DrawType.
        /// </summary>
        [Tooltip("How to draw the gizmos for this script.")]
        [SerializeField]
        private ObjectSizerGizmosDrawTypes _drawType = ObjectSizerGizmosDrawTypes.Vector;
        /// <summary>
        /// How to draw the gizmos for this script.
        /// </summary>
        public ObjectSizerGizmosDrawTypes DrawType { get { return _drawType; } }
        /// <summary>
        /// Size for vector shape.
        /// </summary>
        [Tooltip("Size for vector shape.")]
        [ShowIf("DrawType", ObjectSizerGizmosDrawTypes.Vector)]
        [Indent]
        [SerializeField]
        private Vector3 _vectorSize = new Vector3(1f, 1f, 1f);
        /// <summary>
        /// Size for radius shape.
        /// </summary>
        [ShowIf("DrawType", ObjectSizerGizmosDrawTypes.Radius)]
        [Indent]
        [Tooltip("Size for radius shape.")]
        [SerializeField]
        private float _radiusSize = 1f;
        /// <summary>
        /// True to automatically calculate the center and pivot of the object. Typically used for 3D development.
        /// </summary>
        [Tooltip("True to automatically calculate the center and pivot of the object. Typically used for 3D development.")]
        [SerializeField]
        private bool _autoAlign = false;
        /// <summary>
        /// Color of the indicator.
        /// </summary>
        [Tooltip("Color of the indicator.")]
        [SerializeField]
        private Color _drawColor = Color.magenta;
        /// <summary>
        /// Local offset for the area.
        /// </summary>
        [Tooltip("Local offset for the area.")]
        [SerializeField]
        private Vector3 _offset = Vector3.zero;
        /// <summary>
        /// Use Pivot.
        /// </summary>
        [Tooltip("Offset from the center which is considered the pivot of the ObjectSizer. See updated gizmo as this value changes.")]
        [SerializeField]
        private Vector3 _pivot = Vector3.zero;
        /// <summary>
        /// Offset from the center which is considered the pivot of the ObjectSizer. See updated gizmo as this value changes.
        /// </summary>
        public Vector3 Pivot
        {
            get
            {
                if (_autoAlign)
                    return Center + (new Vector3(0f, -(Size.y / 2f), 0f));
                else
                    return Center + _pivot;
            }
        }
        #endregion

        /// <summary>
        /// Returns a percentage point of where a specified world position exist within the ObjectSizer area.
        /// </summary>
        /// <param name="comparer"></param>
        /// <returns>Axis values are between 0f and 1f.</returns>
        public Vector3 ReturnRelativePoint(Vector3 position)
        {
            Vector3 result = new Vector3(
                Mathf.InverseLerp(Center.x - HalfSize.x, Center.x + HalfSize.x, position.x),
                Mathf.InverseLerp(Center.y - HalfSize.y, Center.y + HalfSize.y, position.y),
                Mathf.InverseLerp(Center.z - HalfSize.z, Center.z + HalfSize.z, position.z)
                );

            return result;
        }

        /// <summary>
        /// Returns a random point within the object sizer.
        /// </summary>
        /// <param name="sizePercentage">Percentage of sizer to use.</param>
        /// <returns></returns>
        public Vector3 ReturnRandomPoint(float sizePercentage = 1f)
        {
            float x = Random.Range(Center.x - (HalfSize.x * sizePercentage), Center.x + (HalfSize.x * sizePercentage));
            float y = Random.Range(Center.y - (HalfSize.y * sizePercentage), Center.y + (HalfSize.y * sizePercentage));
            float z = Random.Range(Center.z - (HalfSize.z * sizePercentage), Center.z + (HalfSize.z * sizePercentage));

            return new Vector3(x, y, z);
        }

        /// <summary>
        /// Sets the AreaSize to a new value.
        /// </summary>
        /// <param name="newSize"></param>
        public void SetSize(Vector3 newSize)
        {
            Size = newSize;
        }
        /// <summary>
        /// Sets a new Offset value.
        /// </summary>
        /// <param name="offset"></param>
        public void SetOffset(Vector3 offset)
        {
            Offset = offset;
        }
        /// <summary>
        /// Sets a new position.
        /// </summary>
        /// <param name="position"></param>
        public void SetPosition(Vector3 position)
        {
            transform.position = position;
        }

        /// <summary>
        /// Returns if another ObjectSizer is within this one's bounds.
        /// </summary>
        /// <param name="otherSizer"></param>
        /// <returns></returns>
        public bool WithinBounds(ObjectSizer otherSizer)
        {
            //If X is out of bounds.
            if (
                ((otherSizer.Center.x - otherSizer.HalfSize.x) > (Center.x + HalfSize.x)) ||
                ((otherSizer.Center.x + otherSizer.HalfSize.x) < (Center.x - HalfSize.x))
                )
                return true;
            //If Y is out of bounds.
            if (
                ((otherSizer.Center.y - otherSizer.HalfSize.y) > (Center.y + HalfSize.y)) ||
                ((otherSizer.Center.y + otherSizer.HalfSize.y) < (Center.y - HalfSize.y))
                )
                return true;

            return false;
        }


        private void OnDrawGizmosSelected()
        {
            Gizmos.color = _drawColor;
            if (DrawType.Contains(ObjectSizerGizmosDrawTypes.Vector))
                Gizmos.DrawWireCube(transform.position + Offset, Size);
            if (DrawType.Contains(ObjectSizerGizmosDrawTypes.Radius))
                Gizmos.DrawWireSphere(transform.position + Offset, Radius);

            //Color pivot.
            Gizmos.color = Color.cyan;
            Gizmos.DrawWireSphere(Pivot, Mathf.Max(Radius, (Size.x + Size.y + Size.z) / 3f) * 0.10f);
        }

    }



}