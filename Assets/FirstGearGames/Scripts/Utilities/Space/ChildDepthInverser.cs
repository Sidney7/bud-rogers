﻿using FirstGearGames.Utilities.Maths;
using FirstGearGames.Utilities.Space.TwoDimensional;
using UnityEngine;

/// <summary>
/// Flips the rotation of specified based on the rotation of the transform which this script resides.
/// This script assumes 0f Y rotation is default.
/// </summary>
public class ChildDepthInverser : MonoBehaviour
{
    #region Serialized.
    /// <summary>
    /// Children transforms to flip when this transform rotation does.
    /// </summary>
    [Tooltip("Children transforms to flip when this transform rotation does.")]
    [SerializeField]
    private Transform[] _childrenToFlip = new Transform[0];
    #endregion

    #region Private.
    /// <summary>
    /// Last direction this transform was known to be facing.
    /// </summary>
    private Directions _lastDirection = Directions.Right;
    #endregion

    private void Awake()
    {
        FirstInitialize();   
    }

    /// <summary>
    /// Initializes the script for first use. Should only be completed once.
    /// </summary>
    private void FirstInitialize()
    {
        if (_childrenToFlip == null || _childrenToFlip.Length == 0)
        {
            Debug.LogError("No children specified for on gameObject " + gameObject.name + ".");
            DestroyImmediate(this);
            return;
        }
    }
    private void Update()
    {
        CheckInverse();
    }

    /// <summary>
    /// Checks to inverse children.
    /// </summary>
    private void CheckInverse()
    {
        Directions currentDirection = transform.ReturnHorizontalFacing();

        //If direction changed.
        if (currentDirection != _lastDirection)
        {
            _lastDirection = currentDirection;

            for (int i = 0; i < _childrenToFlip.Length; i++)
                _childrenToFlip[i].localPosition = _childrenToFlip[i].localPosition.Multiply(new Vector3(0f, 0f, -1f));
        }

    }
}
