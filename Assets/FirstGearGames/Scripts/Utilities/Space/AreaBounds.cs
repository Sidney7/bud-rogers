﻿
using UnityEngine;

namespace FirstGearGames.Utilities.Cameras
{


    /// <summary>
    /// Used to store the bounds of an area.
    /// </summary>
    public class AreaBounds
    {
        public AreaBounds(float left, float right, float top, float bottom)
        {
            Left = left;
            Right = right;
            Top = top;
            Bottom = bottom;
        }

        public readonly float Left;
        public readonly float Right;
        public readonly float Top;
        public readonly float Bottom;

        /// <summary>
        /// Returns the center of the bounds.
        /// </summary>
        public Vector2 Center
        {
            get
            {
                return new Vector2(
                    Mathf.Lerp(Left, Right, 0.5f),
                    Mathf.Lerp(Bottom, Top, 0.5f)
                    );
            }
        }
    }


}