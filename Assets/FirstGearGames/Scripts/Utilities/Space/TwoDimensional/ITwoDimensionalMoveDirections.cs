﻿namespace FirstGearGames.Utilities.Space.TwoDimensional
{

    public interface ITwoDimensionalMoveDirections
    {
        void SetDirection(Directions direction);
    }


}