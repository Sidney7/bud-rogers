﻿using FirstGearGames.Utilities.Maths;
using UnityEngine;

namespace FirstGearGames.Utilities.Space.TwoDimensional
{

    public static class Rotations
    {

        /// <summary>
        /// Returns a rotation to face a specified forward.
        /// </summary>
        /// <param name="forward"></param>
        /// <returns></returns>
        public static Quaternion LookAt(Vector2 forward)
        {
            if (forward == Vector2.zero)
                return Quaternion.identity;
            else
                return Quaternion.Euler(0, 0, Mathf.Atan2(forward.y, forward.x) * Mathf.Rad2Deg);
        }

        /// <summary>
        /// Returns a left or right direction for the specified game objects facing.
        /// </summary>
        /// <param name="go"></param>
        /// <returns></returns>
        public static Directions ReturnHorizontalFacing(this GameObject go)
        {
            return ReturnHorizontalFacing(go.transform);
        }
        /// <summary>
        /// Returns a left or right direction for the specified transforms facing.
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public static Directions ReturnHorizontalFacing(this Transform t)
        {
            if (t.eulerAngles.y.Near(0f, 1f))
                return Directions.Right;
            else
                return Directions.Left;
        }

        /// <summary>
        /// Returns the opposite direction.
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public static Directions ReturnOppositeDirection(Directions direction)
        {
            switch (direction)
            {
                case Directions.Down:
                    return Directions.Up;
                case Directions.Left:
                    return Directions.Right;
                case Directions.None:
                    return Directions.None;
                case Directions.Right:
                    return Directions.Left;
                case Directions.Up:
                    return Directions.Down;
                default:
                    Debug.LogWarning("Unhandled direction type of " + direction.ToString() + ".");
                    return Directions.None;

            }
        }
    }


}